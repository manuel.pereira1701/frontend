import React from 'react'
import Table from "react-bootstrap/Table";



const Tabla = (props) => {
    return (
      <Table striped bordered hover>
        <thead>
          <tr >
            {props.columnas.map((item,index) => (
              <th key={index}>{item}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {props.filas.map((item,index) => {
            return <tr key={index}>
                <td>{item.id}</td>
                <td>{item.disp_nombre}</td>
                <td>{item.modelo.marca.marc_nombre}</td>
                <td>{item.modelo.mode_nombre}</td>
                <td>{item.bodega.bode_nombre}</td>
            </tr>;
          })}
  
        </tbody>
      </Table>
    );
  };

export default Tabla