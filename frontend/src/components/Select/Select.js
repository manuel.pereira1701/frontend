import React, { Fragment } from "react";
import Form from "react-bootstrap/Form"
import "../../styles/Styles.css"



const Select = ({lista,setSelect,nombre}) => {
  return (
    <Form.Select className="Select" aria-label="Default select example" onChange={(event)=>{
      setSelect(event.target.value);
    }}>
      <option value="0">{nombre}</option>
      {
          lista ?
          lista.map((item)=>{
            return <option key={item.id} value={item.id}>{item.label}</option>;
          })
          :<></>
      }
    </Form.Select>
  );
};

export default Select;
