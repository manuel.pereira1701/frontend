import React from 'react'
import "../../styles/Styles.css";


const Contenedor = (props) => {
  return (
    <div className='contenedor'>{props.children}</div>
  )
}

export default Contenedor