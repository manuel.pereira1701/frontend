import React from 'react'
import Contenedor from './Contenedor';
import "../../styles/Styles.css";

const Background = (props) => {
  return (
    <div className='background'>
      <Contenedor>
        {props.children}
      </Contenedor>
    </div>
  )
}

export default Background