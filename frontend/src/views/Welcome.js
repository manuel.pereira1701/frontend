import { useEffect, useState } from "react";
import Tabla from "../components/Table/Tabla";
import Background from "../components/Fondo/Background";
import Select from "../components/Select/Select";
import "./Welcome.css";
import axios from "axios";
import Swal from "sweetalert2";

const Welcome = () => {
  const [marcaSelected, setMarcaSelected] = useState(0);
  const [modeloSelected, setModeloSelected] = useState(0);
  const [bodegaSelected, setBodegaSelected] = useState(0);
  const [marcas, setMarcas] = useState([]);
  const [bodegas, setBodegas] = useState([]);
  const [modelos, setModelos] = useState([]);
  const [dispositivos, setDispositivos] = useState([]);

  useEffect(() => {

    //---------------------Select Marcas y Bodegas-------------//
    axios
      .get("http://127.0.0.1:8000/api/test/listarMarcas")
      .then((res) => setMarcas(res.data))
      .catch((error) => {
        console.log(error.response.data.message);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: "Algo salio mal",
        });
      });

    axios
      .get("http://127.0.0.1:8000/api/test/listarBodegas")
      .then((res) => setBodegas(res.data))
      .catch((error) => {
        console.log(error.response.data.message);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: "Algo salio mal",
        });
      });
  }, []);

  //-------------------Select para los Modelos--------------//

  const marcaSelect = async () => {
    const response = await axios.get(
      `http://127.0.0.1:8000/api/test/listarModelos?marc_id=${marcaSelected}`
    )
    .catch((error) => {
      console.log(error.response.data.message);
      Swal.fire({
        icon: "error",
        title: "Error",
        text: "Algo salio mal",
      });
    });;
    setModelos(response.data);
  };

  useEffect(() => {
    setModeloSelected(0);
    marcaSelect();
  }, [marcaSelected]);

  //------------Datos para la tabla----------//

  const columnas = ["id", "Nombre", "Marca", "Modelo", "Bodega"];

  const bodegaSelect = async () => {
    const response = await axios.get(
      `http://127.0.0.1:8000/api/test/listarPorBodega?bode_id=${bodegaSelected}&marc_id=${marcaSelected}&mode_id=${modeloSelected}`
    )
    .catch((error) => {
      console.log(error.response.data.message);
      Swal.fire({
        icon: "error",
        title: "Error",
        text: "Algo salio mal",
      });
    });;
    setDispositivos(response.data);
  };

  useEffect(() => {
    bodegaSelect();
  }, [bodegaSelected,marcaSelected,modeloSelected]);

  return (
    <Background>
      <div className="container">
        <h1>Dispositivos</h1>
        <div className="contenerSelect">
          <Select lista={marcas} setSelect={setMarcaSelected} nombre="Seleccione Marca"/>
          <Select lista={modelos} setSelect={setModeloSelected} nombre="Seleccione Modelo"/>
        </div>
        <div className="contenerSelect2">
          <Select lista={bodegas} setSelect={setBodegaSelected} nombre="Seleccione Bodega"/>
        </div>
        <Tabla columnas={columnas} filas={dispositivos}/>
      </div>
    </Background>
  );
};

export default Welcome;
