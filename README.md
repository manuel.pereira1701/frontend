# Proyecto en React

Este proyecto va en conjunto con [Backend](https://gitlab.com/manuel.pereira1701/backend)

## Requisitos para el Proyecto

- NodeJs
- npm

## Instalar Proyecto

- clonar el proyecto con `git clone https://gitlab.com/manuel.pereira1701/frontend.git`.
- instalar dependencias `npm i`.


Para Correr el Proyecto usaremos el comando `npm start`
